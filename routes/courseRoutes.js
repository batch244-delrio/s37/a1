const express = require("express")
const router = express.Router();
const courseController = require("../controllers/courseController")
const auth = require("../auth");

// Route for creating a course
router.post("/", auth.verify,(req, res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin) {
		courseController.addCourse(data).then(resultFromController => res.send(resultFromController))
	}else {
		res.send(false)
	}
	
});

//  Retrieving all the courses
router.get("/allCourses", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

// Retrieving all the active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// retrieving specific course
router.get("/:courseId", (req, res) => {

	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// Updating course
// JWT verification is needed to ensure a user is logged in b4 updating a course
router.put("/:courseId",auth.verify,(req, res) => {

	const data = auth.decode(req.headers.authorization);
	
	if (data.isAdmin) {	
		courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));

	} else {
		res.send(false)
	}
});

// Route to archive a course

router.patch("/:courseId/archive", auth.verify, (req, res) => {
	const data = auth.decode(req.headers.authorization);
	if (data.isAdmin){
	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
});


module.exports =  router;