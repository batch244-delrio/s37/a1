const express = require("express");

const router = express.Router();

const auth = require("../auth");

const userController = require("../controllers/userController")

const courseController = require("../controllers/courseController")

// Route for checking if the user's email already exists in the database.
// Invokes the checkEmailExists function from the controller to communicate with our database.

router.post("/checkEmail", (req,res) => {

	// .then method uses the result from the controller funcion sends it back to the frontend application cia res.send method
	 userController.checkEmailExists(req.body).then(resultFromController => res.send (resultFromController));
})

// Route for user  registration 
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Route for userauthentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})


// ACTIVITY---------------------------------------------------------------------------
// The auth.verify acts as a middleware to ensure that the user is login first before they can retrieve the details

router.get("/details", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the auth.js to retrieve the user information from the token, passing the token from the request header as an argument
	const userData = auth.decode(req.headers.authorization);

	// Provides the user ID for the getProfile controller method
	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController))
});

// route to enroll a user to a course

router.post("/enroll",auth.verify, (req, res) => {
	const userData = {
		userId : auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
		
	if (!userData.isAdmin) {	
		userController.enroll(userData).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
});


module.exports = router; 

