const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
		{
			firstName: {
				type: String,
				required: [true, "Please input your Name."]
			},
			lastName: {
				type: String,
				required: [true, "Please input your Last Name."]
			},
			email: {
				type: String,
				required: [true, "Please input your E-mail."]
			},
			password: {
				type: String,
				required: [true, "Please input your password."]
			},
			isAdmin: {
				type: Boolean,
				default: false
			},
			mobileNo: {
				type: String,
				required: [true, "Please input your Mobile Number"]
			},
			enrollments: [
				{
					courseId: {
						type: String,
						required: [true, "Please input Course ID"]
					},
					enrolledOn: {
						type: Date,
						default: new Date()
					},
					status: {
						type: String,
						default: "Enrolled"

					}
				}
			]
		}

	);

module.exports = mongoose.model("User", userSchema);